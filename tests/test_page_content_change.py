import json
import pytest

from eventutilities_python.testing.utils import read_records_from_flink_row_file_sink
from pytest_httpserver import HTTPServer

from mediawiki_event_enrichment.page_content_change import (
    get_config,
    main,
    assert_allowed_revision_size,
    ContentBodyTooLargeError,
)


def test_max_event_size():
    event = {
        "wiki_id": "test_data",
        "meta": {"domain": "test_data"},
        "page": {"page_title": "test_data", "page_id": "page_title"},
        "revision": {"rev_size": 216, "rev_id": "test_data"},
    }

    with pytest.raises(ContentBodyTooLargeError):
        assert_allowed_revision_size(event, 100)

    assert_allowed_revision_size(event, 10_000_000_000)


def test_page_content_change_enrich(
    fixture_stream_manager_default_config_files,
    fixture_stream_manager_defaults,
    httpserver: HTTPServer,
    tmp_path: str,
):
    config = get_config(
        argv=[],  # "--config", fixture_stream_manager_default_config_files[0]],
        default_config_files=fixture_stream_manager_default_config_files,
        defaults=fixture_stream_manager_defaults
        | {
            "enrich.mediawiki_api_endpoint_template": httpserver.url_for(""),
            "enrich.max_rev_size": 2_000_000,
        },
    )

    good_response = {
        "query": {
            "pages": [{"revisions": [{"slots": {"main": {"content": "Test content"}}}]}]
        }
    }
    bad_response = {
        "query": {"badrevids": {"2147483647": {"revid": 2147483647, "missing": True}}}
    }

    maxlag_response = {
        "headers": {
            "date": "Fri, 16 Jun 2023 12:35:52 GMT",
            "server": "mw1356.eqiad.wmnet",
            "retry-after": "5",
            "x-database-lag": "0",
            "mediawiki-api-error": "maxlag",
        },
        "status": 200,
        "body": {
            "error": {
                "code": "maxlag",
                "info": "Waiting for 172.16.3.239:3306: 0 seconds lagged.",
                "host": "172.16.3.239:3306",
                "lag": 0,
                "type": "db",
                "docref": "docref",
            },
            "servedby": "deployment-mediawiki11",
        },
    }

    query_string = "action=query&format=json&formatversion=2&prop=revisions&revids={rev_id}&rvprop=content&rvslots=*&maxlag=5"
    httpserver.expect_request(
        "/", query_string=query_string.format(rev_id=3270050)
    ).respond_with_json(good_response)
    httpserver.expect_request(
        "/", query_string=query_string.format(rev_id=3270051)
    ).respond_with_json(good_response)
    httpserver.expect_request(
        "/", query_string=query_string.format(rev_id=2147483647)
    ).respond_with_json(bad_response)
    httpserver.expect_request(
        "/", query_string=query_string.format(rev_id=12345)
    ).respond_with_json(
        maxlag_response["body"],
        headers=maxlag_response["headers"],
        status=maxlag_response["status"],
    )
    main(config)

    # Read successful and errored events from the configured test file sinks.
    enriched_events = read_records_from_flink_row_file_sink(config.stream_manager.sink)

    # 5 input events, one canary (filtered out) and three errors (written to the error sink).
    assert len(enriched_events) == 2
    for event in enriched_events:
        assert "content_body" in event["revision"]["content_slots"]["main"]
        # meta.id should not be the same as the input events
        assert event["meta"]["id"] not in (
            "1ebb5411-5f86-408a-9324-390f626ddca4",
            "e9b4a51c-a7b9-48f6-8ab4-cbdc14eddacf",
        )

    error_events = read_records_from_flink_row_file_sink(
        config.stream_manager.error_sink
    )
    assert len(error_events) == 3
    expected_error_rev_ids = {2147483647, 12345, 12346}
    error_rev_ids = set()
    for event in error_events:
        errored_event = json.loads(event["raw_event"])
        error_rev_ids.add(errored_event["revision"]["rev_id"])
        if errored_event["revision"]["rev_id"] == 2147483647:
            assert event["error_type"] == "MediaWikiApiMissingContentError"
        elif errored_event["revision"]["rev_id"] == 12345:
            assert event["error_type"] == "MediaWikiApiDatabaseReplicaLagError"
        elif errored_event["revision"]["rev_id"] == 12346:
            assert event["error_type"] == "ContentBodyTooLargeError"

    assert expected_error_rev_ids == error_rev_ids
