import json
import os
from pathlib import Path
from typing import List, Dict, Any

import pytest

# FIXME: I randomly bumped into  https://github.com/apache/flink/pull/17239. Local flink issue?
os.environ["_python_worker_execution_mode"] = "process"


pwd = Path(__file__).parent.resolve()
tests_path = f"{pwd}/tests"


def load_event_files(files: List[str]) -> List[Dict[str, Any]]:
    events = []
    for f in files:
        with open(f, "r") as fp:
            events = events + [json.loads(d) for d in fp.readlines()]
    return events


@pytest.fixture(scope="session")
def fixture_page_change_events_files():
    return [os.path.join(tests_path, "fixture_page_change_events.json")]


@pytest.fixture(scope="session")
def fixture_page_change_events(fixture_page_change_events_files):
    return load_event_files(fixture_page_change_events_files)


@pytest.fixture
def fixture_stream_manager_default_config_files():
    """
    Returns a config file path for use with all python tests.
    """
    return [os.path.join(tests_path, "config.test.defaults.yaml")]


@pytest.fixture
def fixture_stream_manager_defaults(tmp_path, fixture_page_change_events_files):
    """
    Returns a dict of jsonargparse defaults for stream_manager to use
    in tests.  Most importantly the input and output uri paths
    are generated dynamically in tmp_path.
    """

    # Use git submodule of schema repo for tests.
    project_root_path = os.path.dirname(os.path.abspath(__file__))
    schema_uris = [
        os.path.join("file://", project_root_path, "event-schemas", "primary", "jsonschema"),
    ]

    return {
        "stream_manager.schema_uris": schema_uris,
        "stream_manager.source.options": {"uris": fixture_page_change_events_files},
        "stream_manager.sink.options": {"uri": os.path.join(tmp_path, "output")},
        "stream_manager.error_sink.options": {"uri": os.path.join(tmp_path, "errors")},
    }
