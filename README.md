[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)
[![Badge Coverage](https://gitlab.wikimedia.org/repos/data-engineering/mediawiki-event-enrichment/badges/main/coverage.svg)]()

# Mediawiki Event Enrichment 

This repo contains a python implementation of [Mediawiki Stream Enrichment](https://gitlab.wikimedia.org/repos/data-engineering/mediawiki-stream-enrichment/)
based atop [eventutilities_python](https://gitlab.wikimedia.org/repos/data-engineering/eventutilities-python).

![enrichment pattern](https://upload.wikimedia.org/wikipedia/commons/9/97/Mediawiki_Stream_Enrichment_pipeline.png)

This application consumes the `page_change` topic, performs a lookup join with the action API to retrieve raw page content, and produces 
an enriched event into the `page_content_change`.

## Schema Repositories
This project uses a git submodule clone of event schemas, instead of the
remote schema.wikimedia.org service.  

To update the local clone:

```
cd event-schemas/primary
git fetch && git rebase
cd ../..
git add event-schemas/primary
git commit -m "Updated event-schemas/primary git submodule"
```

## Build and test

The application is dockerized. `test` (to run the unit test suite).

```
DOCKER_BUILDKIT=1 docker build --target test -t mediawiki-event-enrichment-test -f .pipeline/blubber.yaml .
```


## References

More information about this application and its deployment can be found at 
https://www.mediawiki.org/wiki/Platform_Engineering_Team/Event_Platform_Value_Stream/Pyflink_Enrichment_Service_Deployment.

## Releasing and building new versions
GitLab CI is configured to use data-engineering/workflow_utils's
[trigger_release CI job](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/tree/main/gitlab_ci_templates#project-versioning),
along with RelEng [Kokkuri's build-and-publish-image CI job](https://gitlab.wikimedia.org/repos/releng/kokkuri#publish-an-image-variant)
to automate tagging and building of docker images for use in WMF production.

In GitLab UI, navigate to a finished pipeline from a commit
on the `main` branch.  Manually run the `trigger_release` job.
This will make a commit to remove the `.dev` suffix from the
version, make a tag at that version, and then make a new
commit to bump the version with a new `.dev` suffix.

The creation of the tag will automatically trigger the
`build-and-publish-image` CI job.  When this job completes
a new docker image with the same version of the created tag
will be published to docker-registry.wikimedia.org.


