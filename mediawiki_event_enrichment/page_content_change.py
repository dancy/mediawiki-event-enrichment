import json
import logging
import os
import time
from typing import Any, Dict, List, Optional

import requests
from eventutilities_python.stream import (
    http_process_function,
    load_config,
    stream_manager,
)
from eventutilities_python.stream.error import error_schema_version
from eventutilities_python.utils import setup_logging
from jsonargparse import ArgumentParser, Namespace
from requests import Response

job_name = "mediawiki_page_content_change_enrich"
"""
Name of this application/job; will be used as default Flink job_name.
"""

mediawiki_api_endpoint_template_default = "https://{domain}/w/api.php"
"""
Default MW API endpoint.  The default is to request page content from the 'meta.domain'
field host in the page_change event.
"""

source_stream_default = "mediawiki.page_change:1.1.0"
"""
Default source EventStreamDescriptor.
"""

sink_stream_default = "mediawiki.page_content_change:1.1.0"
"""
Default sink EventStreamDescriptor.
"""

error_sink_stream_default = (
    f"mw_page_content_change_enrich.error:{error_schema_version}"
)
"""
Default error_sink EventStreamDescriptor.
"""


class MediaWikiApiMissingContentError(Exception):
    """
    Represents an encountered error or unexpected response from the MediaWiki HTTP API.
    """

    pass


class MediaWikiApiDatabaseReplicaLagError(Exception):
    """
    Represents an encountered error or unexpected response from the MediaWiki HTTP API.
    """

    pass


class ContentBodyTooLargeError(Exception):
    """
    The enriched message content exceeds the maximum allowed size (bytes).
    """

    pass


def assert_allowed_revision_size(event: dict, max_rev_size: int):
    """
    Check that the byte sum of the sizes of all content slots
    in this event will not exceed `enrich.max_rev_size`.

    :param event: original event
    :param max_rev_size: max allowed content size (bytes)
    :return:
    """
    rev_size = int(event["revision"]["rev_size"])
    if not rev_size < max_rev_size:
        logging.error(
            "Revision content body exceeds max allowed size (bytes)",
            extra=get_event_log_labels(event),
        )
        raise ContentBodyTooLargeError(
            f"Enriched event rev_size ({rev_size}) exceeds enrich.max_rev_size ({max_rev_size})"
        )


def page_content_api_url(
    revision_id: int,
    domain: str,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    rvslots: str = "*",
    maxlag: int = 5,
) -> str:
    endpoint = mediawiki_api_endpoint_template.format(domain=domain)
    return (
        f"{endpoint}?action=query&format=json&formatversion=2&prop=revisions&"
        f"revids={revision_id}&rvprop=content&rvslots={rvslots}&maxlag={maxlag}"
    )


def retry_response(
    http_session: requests.Session,
    url: str,
    headers: dict = None,
    verify: Optional[str] = None,
    timeout: int = 1,
    max_retries: int = 3,
    wait_on_retry: int = 5,  # TODO: make this a cli / config option
):
    """
    Retry a request on succesfull status codes.

    There are cases when Mediawiki returns 200 status on errors.
    See https://phabricator.wikimedia.org/T33156 for an example.

    This method wraps `requests.Session.get`. and `eventutiliites-python`
    default retry logic, to allow retry on 200s.

    :param http_session: an instance of `requests.Session`
    :param url: url to retrieve
    :param headers: headers to be sent
    :param verify: SSL verification (ca bundle path)
    :param timeout: read timeout
    :param max_retries: max number of retries when the requests fails
    :param wait_on_retry: time (in seconds) to wait between retries
    :return:
    """
    response = http_session.get(
        url,
        headers=headers,
        verify=verify,
        timeout=timeout,
    )
    # We are hitting an API that exposes replicated database. If the replica is lagging,
    # we might get no content. If the API responds with `mediawiki-api-error: maxlag`
    # error, the request should be retried.
    # https://www.mediawiki.org/wiki/Manual:Maxlag_parameter
    # https://phabricator.wikimedia.org/T33156
    if (
        response.status_code == 200
        and (
            response.headers.get("mediawiki-api-error", "") == "maxlag"
            or "badrevids" in response.json()["query"]
        )
        and max_retries > 0
    ):
        # When retrying on 200 status codes, the adapter Retry logic won't be triggered.
        # In this case we call `retry_after` recursively to issue
        # a new request with http_session.get.
        # A `maxlag` response will have status code 200 and a `retry-after` header
        # that advertises how long the application should wait before retrying.
        # If present, we'll use that value as timeout for the new http_session.get.
        wait_on_retry = int(response.headers.get("retry-after", wait_on_retry))
        # TODO: should we pause the thread for a linear time,
        #  or enforce retry logic on 200s in http_session.get ?
        # TODO: what happens if a maxlag is followed by a badrevids? Do
        #  we risk to wait too long / too little by overwriting the default wait_on_retry.
        time.sleep(wait_on_retry)
        return retry_response(
            http_session, url, headers, verify, timeout, max_retries - 1, wait_on_retry
        )
    return response


def get_page_content_response(
    revision_id: int,
    domain: str,
    http_session: requests.Session,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    ssl_ca_bundle_path: Optional[str] = None,
    request_timeout: int = 1,
    wait_on_retry: int = 5,
) -> Response:
    headers = {
        # We must advertise to Mediawiki that this request
        # is non-human traffic (bot).
        "user-agent": f"{job_name} bot",
        "host": domain,
    }

    url = page_content_api_url(revision_id, domain, mediawiki_api_endpoint_template)

    adapter = http_session.adapters.get("https://")
    # eventuilities-python helpers will instantiate a Retry, but a user could override
    # that by supplying a Session() with different adapter logic. If not an instance
    # of Retry, `adapter.max_retries` will be an int that defaults to 0 if not
    # set by the user.
    max_retries = adapter.max_retries
    if isinstance(max_retries, requests.adapters.Retry):
        max_retries = max_retries.total

    return retry_response(
        http_session,
        url,
        headers=headers,
        verify=ssl_ca_bundle_path,
        timeout=request_timeout,
        max_retries=max_retries,
        wait_on_retry=wait_on_retry,
    )


def enrich_page_change_with_content(
    event: dict,
    http_session: requests.Session,
    mediawiki_api_endpoint_template: str = mediawiki_api_endpoint_template_default,
    ssl_ca_bundle_path: Optional[str] = None,
    request_timeout: int = 1,
    max_rev_size: int = 8_000_000,
    wait_on_retry: int = 5,
) -> dict:
    # Ideally we'd want to filter events out early on (e.g. stream.filter(...) in the app
    # context. However, `filter()` currently does not allow redirecting to a side output
    # (the error stream). Raising an exception here guarantees that the originating event
    # will be produced in the error stream.
    #
    # I did consider moving this logic to eventutilities-python, but it's not obvious
    # where a good place would be.
    # We would need to inspect message size before committing to Kafka which means:
    # - we could check size in `EventProcessFunction`: but it's not obvious
    # that we should always drop the message there. Downstream operators might
    # still want to consume large messages before writing into a sink.
    # - before accessing the sink, or serializing Python -> JVM:
    # but we would not be able to forward to a sideoutput (error stream).
    # - in Java eventutilities: this might require having an ad-hoc FlinkProducer,
    # which seems overkill given the number of uses cases we have right now.
    assert_allowed_revision_size(event, max_rev_size)

    # TODO: if we only return content for edit and create, we will
    # not be able to (easily) maintain a compacted topic with content.
    if event["page_change_kind"] in ["edit", "create"]:
        revision_id = event["revision"]["rev_id"]
        domain = event["meta"]["domain"]

        response = get_page_content_response(
            revision_id=revision_id,
            domain=domain,
            http_session=http_session,
            mediawiki_api_endpoint_template=mediawiki_api_endpoint_template,
            ssl_ca_bundle_path=ssl_ca_bundle_path,
            request_timeout=request_timeout,
            wait_on_retry=wait_on_retry,
        )

        # Raise an HTTPError if http status is not 2xx
        response.raise_for_status()

        response_body = response.json()

        # Handle the common case where the revision was not available from the MW API.
        # This happens when the page or revision is removed soon after the page change
        # event happens, when a database replica is lagging, or when maintenance scripts
        # are executed on a database.
        # In this case we should raise an exception and the event will be forwarded to
        # the error topic.
        if response.headers.get("mediawiki-api-error", "") == "maxlag":
            log_event_enrich_request_error(event, response, response_body)
            # Raise an exception so the stream manager error handler can emit an error event.
            # NOTE: If error_sink is not enabled, this exception will be raised up the stack
            # and the job will fail.
            raise MediaWikiApiDatabaseReplicaLagError(
                "Failed requesting %s content from %s. Response body:\n%s"
                % (domain, response.request.url, response_body),
            )
        elif "badrevids" in response_body["query"]:
            log_event_enrich_request_error(event, response, response_body)
            # Raise an exception so the stream manager error handler can emit an error event.
            # NOTE: If error_sink is not enabled, this exception will be raised up the stack
            # and the job will fail.
            raise MediaWikiApiMissingContentError(
                "Failed requesting %s content from %s. Response body:\n%s"
                % (domain, response.request.url, response_body),
            )
        else:
            slots = response_body["query"]["pages"][0]["revisions"][0]["slots"]

            # Iterate through each of the returned revision slots
            # and enrich the page_content_change event.
            for slot_name, slot_data in slots.items():
                if "content" in slot_data:
                    event["revision"]["content_slots"][slot_name][
                        "content_body"
                    ] = slot_data["content"]

    # Remove meta.id so it can be regenerated from eventutilities.
    event["meta"].pop("id", "")

    return event


def get_event_log_labels(event: dict):
    # labels.domain is the wiki domain set in Host header.
    # might differ from `url.full` domain.
    return {
        "labels.domain": event["meta"]["domain"],
        "labels.wiki_id": event["wiki_id"],
        "labels.page_id": event["page"]["page_id"],
        "labels.page_title": event["page"]["page_title"],
        "labels.rev_id": event["revision"]["rev_id"],
    }


def log_event_enrich_request_error(
    event: dict,
    response: requests.Response,
    response_body: dict,
):
    log_labels = get_event_log_labels(event)
    logging.error(
        "Failed requesting %s content from %s",
        event["meta"]["domain"],
        response.request.url,
        extra={
            "http.response.body.content": response_body,
            "url.full": response.request.url,
        }.update(log_labels),
    )


arg_parser = ArgumentParser(
    prog="mediawiki-page-content-change-enrichment",
    description="Enriches mediawiki/page/change events with raw wiki content using MW API:Revisions",
)
arg_parser.add_argument(
    "--enrich.mediawiki_api_endpoint_template",
    default=mediawiki_api_endpoint_template_default,
    help="A python format string template for the MW Action API endpoint to use.  "
    "Only `domain` is supported in the template, and will be extracted from the input event.",
)
arg_parser.add_argument(
    "--enrich.ssl_ca_bundle_path",
    help="Path to the SSL CA bundle to use when making HTTPS requests.",
)
arg_parser.add_argument(
    "--enrich.request_timeout",
    type=int,
    default=1,
    help="MW API request timeout in seconds",
)
arg_parser.add_argument(
    "--enrich.max_rev_size",
    type=int,
    default=8_000_000,
    help="Max allowed size (bytes) of a revision `rev_size`. Events whose "
    "payload exceeds max_rev_size won't be enriched",
)
# Since we will be using @http_process_function to decorate
# our http enrich process function, add params for it.
arg_parser.add_function_arguments(
    http_process_function,
    nested_key="http_session",
)

arg_parser_defaults = {
    "enrich.mediawiki_api_endpoint_template": mediawiki_api_endpoint_template_default,
    "stream_manager.job_name": job_name,
    "stream_manager.source.stream": source_stream_default,
    "stream_manager.sink.stream": sink_stream_default,
    "stream_manager.error_sink.stream": error_sink_stream_default,
    "http_session.pool_maxsize": 12,
}


def get_config(
    argv: List[str] = None,
    defaults: Optional[Dict[Any, Any]] = None,
    default_config_files: Optional[List[str]] = None,
) -> Namespace:
    defaults = defaults or {}

    return load_config(
        argv=argv,
        parser=arg_parser,
        defaults={**arg_parser_defaults, **defaults},
        default_config_files=default_config_files,
    )


def main(config: Namespace = None) -> None:
    config = config or get_config()

    @http_process_function(**config.http_session)
    def enrich(event: dict, http_session: requests.Session) -> dict:
        return enrich_page_change_with_content(
            event=event,
            http_session=http_session,
            **config.enrich,
        )

    with stream_manager.from_config(config) as stream:
        # Filter out canary events
        stream.filter(
            lambda e: e["meta"]["domain"] != "canary",
            name="remove_canary_events",
        )

        # The mediawiki.page_change.v1 stream is produced
        # by eventgate to Kafka with a JSON blob key that
        # includes page_id and wiki_id.
        stream.partition_by(lambda e: (e["wiki_id"], e["page"]["page_id"]))
        stream.process(enrich, name="enrich_with_page_content")
        stream.execute()


if __name__ == "__main__":
    setup_logging(level=os.environ.get("LOG_LEVEL", "INFO"))
    main()
